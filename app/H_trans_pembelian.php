<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class H_trans_pembelian extends Model
{
    protected $table = 'H_trans_pembelians';

    protected $fillable = [
        'no_bukti_bm', 'tanggal_bm', 'no_faktur', 'tanggal_faktur', 'nama_supplier', 'tanggal_jt', 'tipe_diskon','diskon_total', 'total_faktur', 'total_bayar', 'sisa', 'catatan', 'nama_operator'
    ];
}
