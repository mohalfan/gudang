<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bayar extends Model
{
    protected $table = 'bayars';

    protected $fillable = [
        'id_head', 'tanggal_bayar', 'jumlah_bayar', 'nama_operator', 'catatan'
    ];

}
