<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\M_customer;

class CustomerController extends Controller
{
    public function formcustomerpage(){
    	return view('formcustomer');
    }

    public function savecustomer(Request $req){
    	M_customer::create([
    		'no_mitra' => $req->nomitra,
    		'nama' => $req->nama,
    		'email' => $req->email,
    		'alamat' => $req->alamat,
    		'telepon' => $req->telepon,
    		'seluler' => $req->seluler,
    		'website' => $req->web,
    		'catatan' => $req->catatan,
    	]);
    	return redirect('/formcustomer');
    }

    public function listcustomerpage(){
    	$data['customer'] = M_customer::all();
    	return view('listcustomer', $data);
    }

    public function editcustomerpage($id){
    	$data['customer'] = M_customer::find($id)->get();
    	return view('editcustomer', $data);
    }

    public function saveeditcustomer($id){
    	M_customer::where('id',$id)->update([
    		'no_mitra' => $_POST['nomitra'], 
    		'nama' => $_POST['nama'], 
    		'email' => $_POST['email'], 
    		'alamat' => $_POST['alamat'], 
    		'telepon' => $_POST['telepon'], 
    		'seluler' => $_POST['seluler'], 
    		'website' => $_POST['web'], 
    		'catatan' => $_POST['catatan']
    	]);
		return redirect('/listcustomer')->with('message', "Update Data Berhasil");
    }
}
