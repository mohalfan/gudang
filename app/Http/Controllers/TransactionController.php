<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\D_trans_pembelian;
use App\H_trans_pembelian;
use App\M_supplier;
use App\M_item;
use App\Bayar;
use DB;

class TransactionController extends Controller
{
	public function formtransaksipage(){
		$data['supplier'] = M_supplier::all();
		$data['item'] = M_item::all();
		return view('formtransaksi', $data);
	}

	public function savetransaksi(){
		$id = H_trans_pembelian::create([
			'no_bukti_bm' => $_POST['no_bukti_bm'],
			'tanggal_bm' => $_POST['tanggal_bm'],
			'no_faktur' => $_POST['no_faktur'],
			'tanggal_faktur' => $_POST['tanggal_faktur'],
			'nama_supplier' => $_POST['nama_supplier'],
			'tanggal_jt' => $_POST['tanggal_jt'],
			'tipe_diskon' => $_POST['tipe_diskon'],
			'diskon_total' => $_POST['total_diskon'],
			'total_faktur' => $_POST['total_faktur'],
			'total_bayar' => $_POST['total_bayar'],
			'sisa' => $_POST['sisa'],
			'catatan' => $_POST['catatan'],
			'nama_operator' => $_POST['nama_operator'],
		])->id;
		// echo $id;
		$detail = json_decode($_POST['detail'][0]);
		foreach ($detail as $key) {
			D_trans_pembelian::create([
				'id_head' => $id,
				'nama_barang' => $key->namabarang,
				'harga_beli' => $key->hargabeli,
				'qty' => $key->quantity,
				'satuan' => $key->satuan,
				'tipe_diskon' => $key->tipediskon,
				'diskon' => $key->diskon,
				'jumlah' => $key->jumlah,
			]);
			// echo $key->namabarang;
		}

		Bayar::create([
			'id_head' => $id,
			'tanggal_bayar' => $_POST['tanggalbayar'],
			'jumlah_bayar' => $_POST['total_bayar'],
			'nama_operator' => $_POST['nama_operator'],
			'catatan' => $_POST['catatan'],
		]);

		return redirect('/formtransaksi');
	}

	public function getharga(){
		$data = DB::table('m_items')->select('harga_beli', 'unit')->where('nama_produk', $_POST['namabarang'])->get();
    	// $data['satuan'] = DB::table('m_items')->where('nama_produk', $_POST['namabarang']);
		echo json_encode($data);

	}
}
