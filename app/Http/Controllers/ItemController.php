<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\M_item;
use DB;

class ItemController extends Controller
{
    public function formitempage(){
    	$data['kategori'] = Kategori::all();
    	return view('formitem', $data);
    }

    public function saveitem(Request $req){
    	$hasil = M_item::create([
    		'kode_produk' => $req->kode,
    		'nama_produk' => $req->nama,
    		'deskripsi' => $req->deskripsi,
    		'kategori' => $req->kategori,
    		'unit' => $req->unit,
    		'harga_beli' => $req->hargabeli,
    		'harga_jual' => $req->hargajual,
    		'stok_awal' => $req->stokawal,
    		'stok_akhir' => $req->stokakhir,
    	]);
    	return redirect('/formitem');
    }

    public function listitempage(){
    	$data['item'] = DB::table('m_items')->join('kategoris', 'm_items.kategori', '=', 'kategoris.id')->get();
    	return view('listitem', $data);
    }

    public function edititempage($id){
    	$data['item'] = M_item::find($id)->get();
    	$data['kategori'] = Kategori::all();
    	return view('edititem', $data);
    }

    public function saveedititem($id){
    	M_item::where('id',$id)->update([
    		'kode_produk' => $_POST['kode'],
    		'nama_produk' => $_POST['nama'],
    		'deskripsi' => $_POST['deskripsi'],
    		'kategori' => $_POST['kategori'],
    		'unit' => $_POST['unit'],
    		'harga_beli' => $_POST['hargabeli'],
    		'harga_jual' => $_POST['hargajual'],
    		'stok_awal' => $_POST['stokawal']
    	]);
		return redirect('/listitem')->with('message', "Update Data Berhasil");
    }
}
