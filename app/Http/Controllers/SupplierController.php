<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\M_supplier;

class SupplierController extends Controller
{
    public function formsupplierpage(){
    	return view('formsupplier');
    }

    public function savesupplier(Request $req){
    	M_supplier::create([
    		'no_mitra' => $req->nomitra,
    		'nama' => $req->nama,
    		'email' => $req->email,
    		'alamat' => $req->alamat,
    		'telepon' => $req->telepon,
    		'seluler' => $req->seluler,
    		'website' => $req->web,
    		'catatan' => $req->catatan,
    	]);
    	return redirect('/formsupplier');
    }

    public function listsupplierpage(){
    	$data['supplier'] = M_supplier::all();
    	return view('listsupplier', $data);
    }

    public function editsupplierpage($id){
    	$data['supplier'] = M_supplier::find($id)->get();
    	return view('editsupplier', $data);
    }

    public function saveeditsupplier($id){
    	M_supplier::where('id',$id)->update([
    		'no_mitra' => $_POST['nomitra'], 
    		'nama' => $_POST['nama'], 
    		'email' => $_POST['email'], 
    		'alamat' => $_POST['alamat'], 
    		'telepon' => $_POST['telepon'], 
    		'seluler' => $_POST['seluler'], 
    		'website' => $_POST['web'], 
    		'catatan' => $_POST['catatan']
    	]);
    	return redirect('/listsupplier')->with('message', "Update Data Berhasil");
    }
}
