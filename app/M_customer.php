<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_customer extends Model
{
    protected $table = 'm_customers';

    protected $fillable = [
        'no_mitra', 'nama', 'email', 'alamat', 'telepon', 'seluler', 'website','catatan'
    ];
}
