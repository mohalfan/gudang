<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_item extends Model
{
    protected $table = 'm_items';

    protected $fillable = [
        'kode_produk', 'nama_produk', 'deskripsi', 'kategori', 'unit', 'harga_beli', 'harga_jual', 'stok_awal', 'stok_akhir',
    ];
}
