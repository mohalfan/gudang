<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class D_trans_pembelian extends Model
{
    protected $table = 'D_trans_pembelians';

    protected $fillable = [
        'id_head', 'nama_barang', 'harga_beli', 'qty', 'satuan', 'tipe_diskon', 'diskon', 'jumlah'
    ];
}
