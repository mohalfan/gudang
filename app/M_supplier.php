<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_supplier extends Model
{
    protected $table = 'm_suppliers';

    protected $fillable = [
        'no_mitra', 'nama', 'email', 'alamat', 'telepon', 'seluler', 'website','catatan'
    ];
}
