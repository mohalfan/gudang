@extends('layouts.header')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6" style="padding-left: 0;">
                <h4 class="m-0 text-dark">Form tambah item</h4>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Form Item</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>
<div class="row">
    <div class="card col-lg-12" style="padding-top: 15px; padding-bottom: 15px;">
        <div class="container">
            <form action="{{ route('saveitem') }}" method="post">
                {{ csrf_field() }}
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Kode Produk</label>
                    <input type="text" name="kode" class="form-control" placeholder="contoh : SKU0001">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Nama Produk</label>
                    <input type="text" name="nama" class="form-control" placeholder="contoh : LED TV 40, T-Shirt size S, dll.">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Deskripsi Produk</label>
                    <input type="text" name="deskripsi" class="form-control" placeholder="contoh : Warna hitam, size S, 10kg, dll.">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Kategori Produk</label>
                    <select name="kategori" id="" class="form-control">
                        <?php foreach ($kategori as $key) { ?>
                            <option value="">--Select Here--</option>
                            <option value="{{ $key->id }}">{{ $key->nama_kategori }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Unit</label>
                    <input type="text" name="unit" class="form-control" placeholder="Masukkan unit">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Harga Beli</label>
                    <input type="number" name="hargabeli" class="form-control" placeholder="isi harga, contoh : 1000">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Harga Jual</label>
                    <input type="number" name="hargajual" class="form-control" placeholder="isi harga, contoh : 1000">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Stok Awal</label>
                    <input type="number" name="stokawal" class="form-control" placeholder="isi stok awal, contoh : 10">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Stok Akhir</label>
                    <input type="number" name="stokakhir" class="form-control" placeholder="isi stok akhir, contoh : 10">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px; text-align: center;">
                    <input type="submit" class="btn btn-primary" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection