@extends('layouts.header')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6" style="padding-left: 0;">
                <h4 class="m-0 text-dark">Form tambah transaksi</h4>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Form Transaksi</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>
<div class="row">
    <div class="col-lg-12 card" style="padding-right: 0; padding-left: 0; padding-bottom: 20px; padding-top: 20px;">
        <form style="padding-left: 19px; padding-right: 19px;" action="{{ route('savetransaksi') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="detail[]" id="detail">
            <input type="hidden" name="totalbayar" id="totalbayar">
            <div class="row">
                <div class="col-lg-4">
                    <input type="hidden" name="jml_detail" id="jml_detail">
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">No. Bukti Barang Masuk</label>
                        <input type="text" name="no_bukti_bm" class="form-control" placeholder="Masukkan nomor bukti">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Tanggal Barang Masuk</label>
                        <input type="date" name="tanggal_bm" class="form-control">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">No. Faktur</label>
                        <input type="text" name="no_faktur" class="form-control" placeholder="Masukkan nomor faktur">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Tanggal Faktur</label>
                        <input type="date" name="tanggal_faktur" class="form-control">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Nama Supplier</label>
                        <select name="nama_supplier" id="" class="form-control">
                            <option value="">--Select Here--</option>
                            <?php foreach ($supplier as $key) { ?>
                                <option value="{{ $key->nama }}">{{ $key->nama }}</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Tanggal Jatuh Tempo</label>
                        <input type="date" name="tanggal_jt" class="form-control">
                    </div>

                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Tipe Diskon</label>
                        <input type="text" name="tipe_diskon" class="form-control" placeholder="Masukkan tipe diskon">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Diskon Total</label>
                        <input type="number" name="total_diskon" class="form-control" placeholder="Masukkan diskon total">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Total Faktur</label>
                        <input type="text" name="total_faktur" class="form-control" placeholder="Masukkan total faktur">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Tanggal Bayar</label>
                        <input type="date" name="tanggalbayar" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Total Bayar</label>
                        <input type="text" name="total_bayar" class="form-control" placeholder="Masukkan total bayar">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Sisa</label>
                        <input type="text" name="sisa" class="form-control" placeholder="Masukkan Sisa">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Catatan</label>
                        <input type="text" name="catatan" class="form-control" placeholder="Catatan rekening bank, dll.">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Nama Operator</label>
                        <input type="text" name="nama_operator" class="form-control" placeholder="Masukkan nama operator">
                    </div>
                    <div style="padding-top: 35px; padding-bottom: 5px; text-align: center;">
                        <input type="submit" class="btn btn-primary" value="Simpan" onclick="setdetail()">
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 40px;">
                <div class="col-lg-3">
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Nama Barang</label>
                        <select name="namabarang" id="namabarang" class="form-control" onchange="myfunction()">
                            <option value="">--Select Here--</option>
                            <?php foreach ($item as $key) { ?>
                                <option value="">{{$key->nama_produk}}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Harga Beli</label>
                        <input type="text" name="hargabeli" id="hargabeli" class="form-control" placeholder="Masukkan harga beli">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Quantity</label>
                        <input type="text" name="qty" id="qty" class="form-control" placeholder="Masukkan quantity">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Satuan</label>
                        <input type="text" name="satuan" id="satuan" class="form-control" placeholder="Masukkan satuan">
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Tipe Diskon</label>
                        <select name="tipediskon" id="tipediskon" class="form-control">
                            <option value="">--Select Here--</option>
                            <option value="nominal">Nominal</option>
                            <option value="persen">Persen</option>
                        </select>
                    </div>
                    <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Diskon</label>
                        <input type="text" name="diskon" id="diskon" class="form-control" placeholder="Masukkan diskon" onkeyup="jumlah()">
                    </div>
                    <!-- <div style="padding-top: 5px; padding-bottom: 5px;">
                        <label for="">Jumlah</label>
                        <input type="text" name="jumlah" id="jumlah" class="form-control" placeholder="Masukkan Jumlah">
                    </div> -->
                    <div style="padding-top: 35px; padding-bottom: 5px; text-align: center;">
                        <input type="button" class="btn btn-success" value="Tambah" onclick="tambah()">
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="container">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr style="text-align: center;">
                                    <th>No.</th>
                                    <th>Nama Barang</th>
                                    <th>Harga Beli</th>
                                    <th>Quantity</th>
                                    <th>Satuan</th>
                                    <th>Tipe Diskon</th>
                                    <th>Diskon</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody id="rekap" style="text-align: center;">
                            </tbody>
                            <tfoot id="total" style="text-align: center;">

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    var data = [];
    var room = 1;
    var jumlah = 0;
    var disc = 0;
    var kotor = 0;
    var totalkotor = 0;

    function setdetail(){
        document.getElementById('detail').value = JSON.stringify(data);
        document.getElementById('totalbayar').value = jumlah;
    }

    function remove(){
        room--;
    }

    function myfunction(){
        $.ajax({
            url: '{{ route('getharga') }}',
            type: 'POST',
            dataType: 'text',
            data:{
                'namabarang' : $("#namabarang option:selected").html(),
                '_token': "{{ csrf_token() }}",
            },
            success: function(result){
                var hasil = JSON.parse(result);
                console.log(JSON.parse(result));
                document.getElementById('hargabeli').value = parseInt(hasil[0].harga_beli);
                document.getElementById('satuan').value = hasil[0].unit;
            }
        });
    }

    function tambah(){
        var total = 0;
        if (confirm("Yakin akan menambahkan data?") == true) {
            var namabarang = $("#namabarang option:selected").html();
            var hargabeli = document.getElementById("hargabeli").value;
            var quantity = document.getElementById("qty").value;
            var satuan = document.getElementById("satuan").value;
            var tipediskon = $("#tipediskon option:selected").html();
            var diskon = document.getElementById("diskon").value;
            // var jumlah = document.getElementById("jumlah").value;
            if (document.getElementById("tipediskon").value == "nominal") {
                total += hargabeli*quantity;
                kotor += (hargabeli * quantity) - diskon;
                jumlah += total;
                disc += parseInt(diskon);
                totalkotor += total;
            }else if (document.getElementById("tipediskon").value == "persen") {
                total += hargabeli*quantity;
                kotor += (hargabeli * quantity) - (hargabeli * quantity * (diskon/100));
                jumlah += total;
                disc += parseInt((hargabeli * quantity) * (diskon/100));
                totalkotor += total;
            }
            // var jumlah = document.getElementById("jumlah").value;

            data.push({"namabarang":namabarang,"hargabeli":hargabeli,"quantity":quantity,"satuan":satuan,"tipediskon":tipediskon,"diskon":diskon,"jumlah":total});
            console.log(data);

            var objTo = document.getElementById('rekap')
            var divtest = document.createElement("tr");
            divtest.innerHTML = 
            "<tr>"+
            "<td>"+(room++)+"</td>"+
            "<td>"+namabarang+"</td>"+
            "<td>"+hargabeli+"</td>"+
            "<td>"+quantity+"</td>"+
            "<td>"+satuan+"</td>"+
            "<td>"+tipediskon+"</td>"+
            "<td>"+diskon+"</td>"+
            "<td>"+total+"</td>"+
            "</tr>";
            objTo.appendChild(divtest);

            var htmljumlah = 
            "<tr>"+
            "<td colspan='7'>Total Biaya</td>"+
            "<td>"+totalkotor+"</td>"+
            "</tr>"+
            "<tr>"+
            "<td colspan='7'>Total Diskon</td>"+
            "<td>"+disc+"</td>"+
            "</tr>"+
            "<td colspan='7'>Total Biaya Akhir</td>"+
            "<td>"+kotor+"</td>"+
            "</tr>"
            ;
            document.getElementById("total").innerHTML = htmljumlah;

            document.getElementById("hargabeli").value = "";
            document.getElementById("qty").value = "";
            document.getElementById("satuan").value = "";
            document.getElementById("diskon").value = "";
            document.getElementById("namabarang").value = "";
            document.getElementById("tipediskon").value = "";
        };
    }
</script>
@endsection