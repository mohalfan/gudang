@extends('layouts.header')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6" style="padding-left: 0;">
                <h4 class="m-0 text-dark">Form tambah customer</h4>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Form Customer</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>
<div class="row">
    <div class="card col-lg-12" style="padding-top: 15px; padding-bottom: 15px;">
        <div class="container">
            <form action="{{ route('savecustomer') }}" method="post">
                {{ csrf_field() }}
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">No. Mitra</label>
                    <input type="number" name="nomitra" class="form-control" placeholder="contoh : 0001">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Nama</label>
                    <input type="text" name="nama" class="form-control" placeholder="contoh : PT. ABC atau Aditya">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="contoh : nama@email.com">
                    <p style="font-weight: bold; font-style: italic;">Tips : Isi kolom diatas untuk mengirim invoice secara instan ke mitra anda</p>
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Alamat</label>
                    <input type="text" name="alamat" class="form-control" placeholder="contoh : Jl. in aja dulu No.69 ....">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Telepon</label>
                    <input type="number" name="telepon" class="form-control" placeholder="contoh : 021...">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Telepon Seluler</label>
                    <input type="number" name="seluler" class="form-control" placeholder="contoh : 0821...">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Website</label>
                    <input type="text" name="web" class="form-control" placeholder="contoh : www.abc.com">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Catatan</label>
                    <input type="text" name="catatan" class="form-control" placeholder="Catatan rekening bank, dll.">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px; text-align: center;">
                    <input type="submit" class="btn btn-primary" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection