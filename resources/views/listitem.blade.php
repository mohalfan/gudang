@extends('layouts.header')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6" style="padding-left: 0;">
                <h4 class="m-0 text-dark">List Barang</h4>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">List Barang</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>
@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
<div class="row">
    <div class="card col-lg-12" style="padding-top: 15px; padding-bottom: 15px;">
        <div class="container">
            <table class="table table-striped">
                <thead>
                    <tr style="text-align: center;">
                        <th>No.</th>
                        <th>Kode Produk</th>
                        <th>Nama Produk</th>
                        <th>Nama Kategori</th>
                        <th>Harga Jual</th>
                        <th>Harga Beli</th>
                        <th>Stok Akhir</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($item->isEmpty()) { ?>
                        <tr style="text-align: center;">
                            <p>Tidak Ada Data</p>
                        </tr>
                    <?php } else { ?>
                    <?php $bil=1; foreach ($item as $key) { ?>
                        <tr style="text-align: center;">
                            <td>{{ $bil++ }}</td>
                            <td>{{ $key->kode_produk }}</td>
                            <td>{{ $key->nama_produk }}</td>
                            <td>{{ $key->nama_kategori }}</td>
                            <td>Rp. {{ number_format($key->harga_jual,0,',','.') }}</td>
                            <td>Rp. {{ number_format($key->harga_beli,0,',','.') }}</td>
                            <td>{{ $key->stok_akhir }}</td>
                            <td>
                                <a href="{{ route('edititem',['id' => $key->id]) }}" class="btn btn-warning" style="color: #fff;">Edit</a>
                                <!-- <a onclick="return confirm('Apakah anda yakin menghapus data {{$key->nama_produk}}')" href="{{ route('deleteitem',['id' => $key->id]) }}" class="btn btn-danger" style="color: #fff;">Delete</a> -->
                            </td>
                        </tr>
                    <?php }} ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection