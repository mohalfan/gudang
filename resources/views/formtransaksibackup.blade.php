@extends('layouts.header')

@section('title', 'Page Title')

@section('sidebar')
@parent
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6" style="padding-left: 0;">
                <h4 class="m-0 text-dark">Form tambah transaksi</h4>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Form Transaksi</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</section>
<div class="row" style="padding-bottom: 20px;">
    <div class="col-lg-12" style="padding-right: 0; padding-left: 0;">
        <form class="col-lg-12 card" style="padding-left: 19px; padding-right: 19px;" action="{{ route('savetransaksi') }}" method="post">
            {{ csrf_field() }}
            <div class="card-header border-transparent">
                <!-- <h3 class="card-title">Header Transaksi</h3> -->
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body p-0">
                <input type="hidden" name="jml_detail" id="jml_detail">
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">No. Bukti Barang Masuk</label>
                    <input type="text" name="no_bukti_bm" class="form-control" placeholder="Masukkan nomor bukti">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Tanggal Barang Masuk</label>
                    <input type="date" name="tanggal_bm" class="form-control">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">No. Faktur</label>
                    <input type="text" name="no_faktur" class="form-control" placeholder="Masukkan nomor faktur">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Tanggal Faktur</label>
                    <input type="date" name="tanggal_faktur" class="form-control">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Nama Supplier</label>
                    <select name="nama_supplier" id="" class="form-control">
                        <option value="">--Select Here--</option>
                        <?php foreach ($supplier as $key) { ?>
                            <option value="{{ $key->nama }}">{{ $key->nama }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Tanggal Jatuh Tempo</label>
                    <input type="date" name="tanggal_jt" class="form-control">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Tipe Diskon</label>
                    <input type="text" name="tipe_diskon" class="form-control" placeholder="Masukkan tipe diskon">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Diskon Total</label>
                    <input type="number" name="total_diskon" class="form-control" placeholder="Masukkan diskon total">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Total Faktur</label>
                    <input type="text" name="total_faktur" class="form-control" placeholder="Masukkan total faktur">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Total Bayar</label>
                    <input type="text" name="total_bayar" class="form-control" placeholder="Masukkan total bayar">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Sisa</label>
                    <input type="text" name="sisa" class="form-control" placeholder="Masukkan Sisa">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Catatan</label>
                    <input type="text" name="catatan" class="form-control" placeholder="Catatan rekening bank, dll.">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px;">
                    <label for="">Nama Operator</label>
                    <input type="text" name="catatan" class="form-control" placeholder="Masukkan nomor operator">
                </div>
                <div style="padding-top: 5px; padding-bottom: 5px; text-align: center;">
                    <input type="submit" class="btn btn-primary" value="Simpan" onclick="setdetail()">
                </div>
            </div>  
        </div>
        <div class="col-lg-12" id="detail" style="padding-right: 0; padding-left: 0;"></div>
    </form>
    <button class="btn btn-success" onclick="addform()">Add detail</button>
</div>
<script>
    var room = 0;
    function addform(){
        room++;
        var objTo = document.getElementById('detail')
        var divtest = document.createElement("div");
        divtest.innerHTML = 
        "<div class='card' id='new'  style='padding-left: 20px; padding-right: 20px'>"+
        "<div class='card-header border-transparent'>"+
        "<h3 class='card-title'>Detail Transaksi "+room+"</h3>"+
        "<div class='card-tools'>"+
        "<button type='button' class='btn btn-tool' data-card-widget='collapse'>"+
        "<i class='fas fa-minus'></i>"+
        "</button>"+
        "</div>"+
        "</div>"+
        "<div class='card-body p-0'>"+
        "<div class='row'>"+
        "<div class='col-lg-6' style='padding-bottom:20px;'>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Nama Barang</label>"+
        "<select name='nama_barang"+room+"' id='namabarang"+room+"' class='form-control' onchange='myfunction("+room+")'>"+
        "<option>--Select Here--</option>"+
        <?php foreach ($item as $key) { ?>
            "<option value={{ $key->nama_produk }}>{{ $key->nama_produk }}</option>"+
        <?php } ?>
        "</select>"+
        "</div>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Harga Beli</label>"+
        "<input type='number' name='harga_beli"+room+"' class='form-control' id='hb"+room+"' placeholder='Masukkan harga beli'>"+
        "</div>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Satuan</label>"+
        "<input type='text' name='satuan"+room+"' id='satuan"+room+"' class='form-control' placeholder='Masukkan satuan'>"+
        "</div>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Diskon</label>"+
        "<input type='number' name='diskon"+room+"' id='diskon"+room+"' class='form-control' placeholder='Masukkan diskon'>"+
        "</div>"+
        "</div>"+
        "<div class='col-lg-6' style='padding-bottom:20px;'>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Jenis Barang</label>"+
        "<input type='text' name='jenis_barang"+room+"' id='jenis_barang"+room+"' class='form-control' placeholder='Masukkan jenis barang'>"+
        "</div>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Quantity</label>"+
        "<input type='number' name='qty"+room+"' id='qty"+room+"' class='form-control' placeholder='Masukkan quantity'>"+
        "</div>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Tipe Diskon</label>"+
        "<select name='tipe_diskon"+room+"' id='tipe_diskon"+room+"' class='form-control'>"+
        "<option>--Select Here--</option>"+
        "<option value='persen'>Persen</option>"+
        "<option value='nominal'>Nominal</option>"+
        "</select>"+
        "</div>"+
        "<div style='padding-top: 5px; padding-bottom: 5px;'>"+
        "<label>Jumlah</label>"+
        "<input type='number' name='jumlah"+room+"' id='jumlah"+room+"' class='form-control' placeholder='Masukkan jumlah'>"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>";
        objTo.appendChild(divtest);
    }

    function setdetail(){
        document.getElementById('jml_detail').value = room;
    }

    function remove(){
        room--;
    }

    function myfunction(param){
        $.ajax({
            url: '{{ route('getharga') }}',
            type: 'POST',
            dataType: 'text',
            data:{
                'namabarang' : $("#namabarang"+param+" option:selected").html(),
                '_token': "{{ csrf_token() }}",
            },
            success: function(result){
                var hasil = JSON.parse(result);
                console.log(JSON.parse(result));
                document.getElementById('hb'+param).value = parseInt(hasil[0].harga_beli);
                document.getElementById('satuan'+param).value = hasil[0].unit;
            }
        });
    }
</script>
@endsection