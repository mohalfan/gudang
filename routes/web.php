<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->name('home');
Route::get('/formitem', 'ItemController@formitempage')->name('formitem');
Route::get('/formsupplier', 'SupplierController@formsupplierpage')->name('formsupplier');
Route::get('/formcustomer', 'CustomerController@formcustomerpage')->name('formcustomer');
Route::post('/saveitem', 'ItemController@saveitem')->name('saveitem');
Route::post('/savesupplier', 'SupplierController@savesupplier')->name('savesupplier');
Route::post('/savecustomer', 'CustomerController@savecustomer')->name('savecustomer');
Route::get('/listitem', 'ItemController@listitempage')->name('listitem');
Route::get('/listsupplier', 'SupplierController@listsupplierpage')->name('listsupplier');
Route::get('/listcustomer', 'CustomerController@listcustomerpage')->name('listcustomer');
Route::get('/edititem/{id}', 'ItemController@edititempage')->name('edititem');
Route::get('/editsupplier/{id}', 'SupplierController@editsupplierpage')->name('editsupplier');
Route::get('/editcustomer/{id}', 'CustomerController@editcustomerpage')->name('editcustomer');
Route::get('/deleteitem/{id}', 'ItemController@deleteitem')->name('deleteitem');
Route::get('/deletesupplier/{id}', 'SupplierController@deletesupplier')->name('deletesupplier');
Route::get('/deletecustomer/{id}', 'CustomerController@deletecustomer')->name('deletecustomer');
Route::post('/saveedititem/{id}', 'ItemController@saveedititem')->name('saveedititem');
Route::post('/saveeditsupplier/{id}', 'SupplierController@saveeditsupplier')->name('saveeditsupplier');
Route::post('/saveeditcustomer/{id}', 'CustomerController@saveeditcustomer')->name('saveeditcustomer');
Route::get('/formtransaksi', 'TransactionController@formtransaksipage')->name('formtransaksi');
Route::post('/savetransaksi', 'TransactionController@savetransaksi')->name('savetransaksi');
Route::post('/getharga', 'TransactionController@getharga')->name('getharga');
